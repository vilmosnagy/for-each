/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.gitlab.vilmosnagy;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MyBenchmark {

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(MyBenchmark.class.getSimpleName())
                .warmupIterations(10)
                .measurementIterations(3)//number of times the actual iteration should take place
                .forks(2)
                .shouldDoGC(true)
                .build();


        new Runner(opt).run();
    }

    private static final List<Integer> OBJECTS =
            IntStream
                    .range(0, 100)
                    .boxed()
                    .collect(Collectors.toList());
    
    private static final List<Integer> ARRAYLIST_OBJECTS =
            new ArrayList<>(OBJECTS);

    private static final Integer[] ARRAY_OBJECTS = ARRAYLIST_OBJECTS.toArray(new Integer[]{});

    public static transient int modCount = 0;

    @Benchmark
    public void streamForEach(Blackhole blackhole) {
        ARRAYLIST_OBJECTS.stream().forEach(blackhole::consume);
    }

    @Benchmark
    public void enhancedForLoop(Blackhole blackhole) {
        for (Integer i : ARRAYLIST_OBJECTS) {
            blackhole.consume(i);
        }
    }

    @Benchmark
    public void arrayForLoop(Blackhole blackhole) {
        for (int i = 0; i < ARRAY_OBJECTS.length; i++) {
            blackhole.consume(ARRAY_OBJECTS[i]);
        }
    }

    @Benchmark
    public void arrayForLoopWithSizeCheck(Blackhole blackhole) {
        final int expectedModCount = modCount;
        for (int i = 0; expectedModCount == modCount && i < ARRAY_OBJECTS.length; i++) {
            blackhole.consume(ARRAY_OBJECTS[i]);
        }

    }

    @Benchmark
    public void noStreamForEach(Blackhole blackhole) {
        ARRAYLIST_OBJECTS.forEach(blackhole::consume);
    }

}
